# Programování v geoinformatice
## Seznam kapitol
Každá kapitola obsahuje odkaz na odpovídající složku v tomto repozitáři a prezentaci na [Slides](https://slides.com)
1. [Úvodní prezentace](https://slides.com/bulva/programovani-v-geoinformatice-uvod)
    * Informace ke cvičením
    * Co je programování
    * Instalace Pythonu a PyCharmu
    * Python
1. [GitLab](https://slides.com/bulva/gitlab)
1. [Datové typy a podmínky](https://slides.com/bulva/datove-typy-a-podminky)

## Kurzy na internetu
* [w3 schools](https://www.w3schools.com/python/)
* [Code Academy](https://www.codecademy.com/learn/learn-python)
* [Learn Python](https://www.learnpython.org/)
