"""
Vaším úkolem je vytvořit textovou hru:
https://cs.wikipedia.org/wiki/Textov%C3%A1_hra

Při tvorbě použijte proměnné, funkci input(), funkci print(), alespoň 4 podmínky (jedna z podmínek bude
obsahovat i elif větev a zanořenou podmínku včetně operatorů and a or).

Můžete udělat hru na jakékoliv téma. Hlavně ať to není nudné a splní to minimální podmínky zadání!

Celkem to bude za 5 bodů. Dalších 5 bonusových bodů (nezapočítávají se do základu)
můžete získat za správné použití 5 libovolných funkcí pro řětězce, čísla (např. split() apod.). Další funkce zkuste vygooglit na internetu.
Použité funkce se musí vztahovat k logice hry.https://gitlab.com/SimonaBockova/hodina_1_sb.git
"""

print('Vidím, že se celý den pekně nudíš, tak si spolu zahrajeme kámen, nůžky, papír... ')
print ('Co ty na to?')
print('Začínáš ty, potom hraju Já, počítač')
print('Tak jo... připraven? Napiš kámen, nůžky nebo papír:')


tah_cloveka = input()

print('teď hraju Já a můj nejchytřejší počítačový tah je: ')

import random

moznosti = ['kámen', 'nůžky', 'papír']

tah_pocitace = random.choice(moznosti)

#print(str(tah_cloveka))
print(str(tah_pocitace))

if tah_cloveka == tah_pocitace:
    print('remíza, jsi stejně chytrý jako počítač, dej si další kolo')
elif tah_pocitace == 'nůžky' and tah_cloveka == 'kámen':
    print('Vyhrál jsi nad počítačem člověče! Gratuluji')
elif tah_pocitace == 'nůžky' and tah_cloveka == 'papír':
    print('Nedá se nic dělat, počítač je chytřejší a vyhrál, zkus to znovu')
elif tah_pocitace == 'kámen' and tah_cloveka == 'papír':
    print('Vyhrál jsi nad počítačem člověče! Gratuluji')
elif tah_pocitace == 'kámen' and tah_cloveka == 'nůžky':
    print('Nedá se nic dělat, počítač je chytřejší a vyhrál, zkus to znovu')
elif tah_pocitace == 'papír' and tah_cloveka == 'kámen':
    print('Vyhrál jsi nad počítačem člověče! Gratuluji')
elif tah_pocitace == 'papír' and tah_cloveka == 'nůžky':
    print('Nedá se nic dělat, počítač je chytřejší a vyhrál, zkus to znovu')
else:
    print('tohle co píšeš ty je sice hezké slovo ale já se tady snažím poctivě hrát')
    print('a vidím, že už se se mnou nudíš nebo neumíš psát na klávesnici,')
    print('tohle nejsou nůžky, papír ani kámen, zkus to raději znovu')